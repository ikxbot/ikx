# Ikx IRC Bot
## Requirements
Ikx IRC Bot has the following requirements:
- PHP 7.2 or later
- MongoDB v3.6 or later
- cURL extension enabled
- MongoDB extension enabled

### Running Ikx on Windows
Due to limitations in PHP's `pcntl` extension, running Ikx on Windows is
currently not supported. Please run Ikx on OSX or Linux. 

## Installation
Installation is simple: just create a project with composer and everything will
be installed automatically. Run the following command to install the Ikx IRC
Bot:
```
composer create-project ikxbot/ikx ikx-irc-bot
```
Ikx IRC bot will be installed in the `ikx-irc-bot` folder.

## Configuration
Ikx IRC Bot uses a YAML formatted configuration file. See
`app/config.example.yml` for a documented configuration example. You can also
use this file to create a configuration file for your own IRC Bot.

### Configuration example
```
defaults:
  # Defaults-block
  # ---------
  # Defines default settings for the bot, which will be shared throughout all networks
  # All settings in defaults can me overridden on network level
  me:
    # Me-block
    # ---------
    # Identifies the bot on the server
    #
    # nickname: the bot's nickname(s) (separate with spaces to select a random nickname when connecting)
    # altnick: the bot's alternative nickname(s) (separate with spaces to select a random nickname when the nickname is taken)
    # username: the bot's username (ident)
    # gecos: the bot's gecos (realname)
    nickname: "Ikx"
    altnick: "Ikx` Ikx_"
    username: "IkxBot"
    gecos: "IkxBot"
  options:
    # Options-block
    # ---------
    # Specify several options for the bot
    #
    # prefix: Command prefix character
    prefix: "-"
networks:
  # Networks-block
  # ---------
  # Specify networks to connect on
  # The networks-block is an array, with every item allowing having serveral requirements
  #
  # identifier: The network identifier
  # address: an array of addresses available for this server
  #          in the format of: <ip>:<+port> <password>
  #          prefix the port with a plus (+) sign to connect using SSL
  #
  # Every network also supports a me-block and an options-block, which allows to override the default settings above.
  # Only the specified settings in these blocks will be overridden.

  - identifier: "Hunter (Dev)"
    address:
      - "hunter.jrv.cc:+6697"
#  - identifier: "DevIRC"
#    address:
#      - "transip.devirc.net:6667"
#    options:
#      prefix: ":"
```

## Running the bot
After you have configured the bot, you may want to let it connect to your
IRC server. Ikx IRC Bot offers an easy console command which allows to
run the bot:
```
php Ikx run
```